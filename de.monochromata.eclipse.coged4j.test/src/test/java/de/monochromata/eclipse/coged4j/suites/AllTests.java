package de.monochromata.eclipse.coged4j.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.monochromata.eclipse.coged4j.preferences.PreferencePageTest;

/**
 * Needs to be run as a JUnit Plug-in Test, but not in the UI thread.
 */
@RunWith(Suite.class)
@SuiteClasses({ PreferencePageTest.class })
public class AllTests {

}
