package de.monochromata.eclipse.coged4j.preferences;

import static org.assertj.core.api.Assertions.assertThat;

import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PreferencePageTest {

    private final SWTBot swtBot = new SWTBot();

    @Before
    public void openPreferences() {
        swtBot.activeShell().menu().menu("Window", "Preferences").click();
    }

    @After
    public void closePreferences() {
        swtBot.button("Cancel").click();
    }

    @Test
    public void preferencePageIsShown() {
        final SWTBotTreeItem preferenceNode = swtBot.tree().getTreeItem("Cognitive Editor");
        preferenceNode.expand();
        preferenceNode.click();

        assertThat(swtBot.label("Configuration of the cognitive editor")).isNotNull();
        assertThat(swtBot.labelWithId("coged4j.featurePreferenceHint")).isNotNull();
        assertThat(swtBot.linkWithId("coged4j.homepageLink")).isNotNull();
        assertThat(swtBot.linkWithId("coged4j.twitterLink")).isNotNull();
    }

    @Test
    public void preferenceTreeNodesAreCorrect() {
        final SWTBotTreeItem cogedItem = swtBot.tree().getTreeItem("Cognitive Editor");
        cogedItem.expand();

        assertThat(cogedItem).isNotNull();
        assertThat(cogedItem.getNode("Anaphors")).isNotNull();
    }

}
