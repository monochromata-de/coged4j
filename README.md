# Cognitive Editor for Java (beta)

[![pipeline status](https://gitlab.com/monochromata-de/coged4j/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/coged4j/commits/master)

A cognitive Java editor for Eclipse that employs theory from cognitive linguistics to make programming work more in the way of how human minds work - without loss of correctness.

The editor shall support clean *code* by
* encouraging descriptive names: the info from the names is used to find referents
* hiding redundant information - if you wish to (change via `Preferences` > `Anaphors`)

The editor shall support clean *coding* by
* automatically introducing (and hiding) local variables for anaphors that refer to referents in the prior code. I.e. it is not necessary to explicitly extract variables. Just refer to what you want to refer to.

See the following demo video.

[![Demo Video](https://i.vimeocdn.com/video/740323420.jpg?mw=390&mh=219)](https://vimeo.com/301708027)

What does *beta* mean here? It gets betta' continuously! The project is still at an early stage. It needs your feedback to improve while releasing early and often. It is best tried when you are in a relaxed, playful mood.

## Install

* In Eclipse, go to `Help` > `Install New Software`
* Add [http://monochromata.de/eclipse/sites/coged4j/](http://monochromata.de/eclipse/sites/coged4j/) as a new update site and follow the instructions
* Select and install the Cognitive Editor / Anaphors for Eclipse.

## Usage

In Project Explorer, right-click on the `.java` file and choose `Open With` > `Other ...`.
In the *Editor Selection* window, select the *internal editor* *Cognitive Editor* and check *Use it for all &ast;.java files*. 
  
  ![Screenshot: configure Cognitive Editor as default editor](docs/images/editorSelection.png)

## Related work

* [Pegasus project](http://www.pegasus-project.org/en/Welcome.html)

## License

EPL 2.0