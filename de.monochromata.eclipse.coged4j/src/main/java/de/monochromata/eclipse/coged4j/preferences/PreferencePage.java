package de.monochromata.eclipse.coged4j.preferences;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.monochromata.eclipse.coged4j.CogEd4J;

public class PreferencePage extends org.eclipse.jface.preference.PreferencePage implements IWorkbenchPreferencePage {

    /**
     * The constant is defined to avoid a dependency on SWTBot and to avoid needing
     * to set a property in the tests to define a custom key.
     */
    private static final String SWTBOT_DEFAULT_KEY = "org.eclipse.swtbot.widget.key";

    public PreferencePage() {
        super("Cognitive Editor");
        setPreferenceStore(CogEd4J.getDefault().getPreferenceStore());
        setDescription("Configuration of the cognitive editor");
        noDefaultAndApplyButton();
    }

    @Override
    protected Control createContents(final Composite parent) {
        final Composite composite = createComposite(parent);
        createLabel(composite, "coged4j.featurePreferenceHint", "Expand the tree for features");
        createLink(composite, "coged4j.homepageLink",
                "Visit the <a href=\"https://gitlab.com/monochromata-de/coged4j\">project homepage</a>");
        createLink(composite, "coged4j.twitterLink",
                "Find project updates at <a href=\"https://twitter.com/monochromataDe\">@monochromataDe</a>");
        return composite;
    }

    protected Composite createComposite(final Composite parent) {
        final Composite composite = new Composite(parent, SWT.NULL);
        final GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        composite.setLayout(layout);
        composite.setFont(parent.getFont());
        return composite;
    }

    protected void createLabel(final Composite composite, final String key, final String text) {
        final Label label = new Label(composite, SWT.NULL);
        label.setData(SWTBOT_DEFAULT_KEY, key);
        label.setText(text);
    }

    protected void createLink(final Composite composite, final String key, final String text) {
        final Link link = new Link(composite, SWT.NULL);
        link.setData(SWTBOT_DEFAULT_KEY, key);
        link.setText(text);
    }

    @Override
    public void init(final IWorkbench workbench) {
    }

}
