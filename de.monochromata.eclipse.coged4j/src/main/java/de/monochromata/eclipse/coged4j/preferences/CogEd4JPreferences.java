package de.monochromata.eclipse.coged4j.preferences;

import org.eclipse.jface.preference.IPreferenceStore;

public class CogEd4JPreferences {

    private final IPreferenceStore preferenceStore;

    public CogEd4JPreferences(final IPreferenceStore preferenceStore) {
        this.preferenceStore = preferenceStore;
    }

}
