package de.monochromata.eclipse.coged4j;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class CogEd4J extends AbstractUIPlugin {

    public static final String PLUGIN_ID = "de.monochromata.eclipse.coged4j";
    private static BundleContext context;
    private static CogEd4J instance;

    public CogEd4J() {
    }

    static BundleContext getContext() {
        return context;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.
     * BundleContext)
     */
    @Override
    public void start(final BundleContext bundleContext) throws Exception {
        CogEd4J.context = bundleContext;
        instance = this;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(final BundleContext bundleContext) throws Exception {
        CogEd4J.context = null;
    }

    public static CogEd4J getDefault() {
        return instance;
    }

}
